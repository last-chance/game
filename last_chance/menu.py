"""Menu"""

import pygame
from . import constants as con
from . import button as btn

class Menu():
    """Menu class"""

    def __init__(self, game):
        self.game = game
        self.display = True
        self.music_playing = False
        self.surf = pygame.Surface(self.game.surf.get_size(), pygame.SRCALPHA)
        self.rect = self.surf.get_rect(topleft=(0, 0))
        self.surf.fill(con.MENU_BACK)
        self.logo = pygame.image.load(con.LOGO)
        self.logo_rect = self.logo.get_rect(center=(con.WIDTH//2, con.HEIGHT//6))
        self.cont_button = btn.Button('Continue', coords=(con.WIDTH//2, con.HEIGHT//3),
            size=(con.MENU_BTN_WIDTH, 0))
        self.new_button = btn.Button('New Game', coords=(con.WIDTH//2, con.HEIGHT//12*5),
            size=(con.MENU_BTN_WIDTH, 0))
        self.settings_keys = ['sounds', 'music', 'fullscreen']
        self.settings_labels = ['Sounds: ', 'Music: ', 'Fullscreen: ']
        self.settings_buttons = []
        for i in range(3):
            button = btn.Button('', coords=(con.WIDTH//2, con.HEIGHT//12*(i+7)),
                size=(con.MENU_BTN_WIDTH, 0))
            self.update_button(button, i)
            self.settings_buttons.append(button)
        self.quit_button = btn.Button('Quit Game', coords=(con.WIDTH//2, con.HEIGHT//12*11),
            size=(con.MENU_BTN_WIDTH, 0))

    def update(self, events):
        """Function for updating menu"""
        if not pygame.mixer.music.get_busy() and self.game.settings['music']:
            pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
            pygame.mixer.music.load(con.INTRO_MUSIC)
            pygame.mixer.music.play(-1)
            self.music_playing = True
        self.surf.fill(con.MENU_BACK)
        self.surf.blit(self.logo, self.logo_rect)
        self.surf.blit(self.cont_button.surf, self.cont_button.rect)
        self.surf.blit(self.new_button.surf, self.new_button.rect)
        self.surf.blit(self.quit_button.surf, self.quit_button.rect)
        if self.cont_button.clicked(events):
            self.display = False
            self.music_playing = False
            pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
        if self.new_button.clicked(events):
            self.game.settings['level'] = 0
            self.game.settings['sublevel'] = 0
            self.display = False
            self.music_playing = False
            pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
        if self.quit_button.clicked(events):
            pygame.event.post(con.QUIT_EVENT)
        for i, button in enumerate(self.settings_buttons):
            if button.clicked(events):
                setting = self.game.settings[self.settings_keys[i]]
                self.game.settings[self.settings_keys[i]] = not setting
                if not self.game.settings['music']:
                    pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
                if self.settings_keys[i] == 'fullscreen':
                    flags = 0
                    if self.game.settings[self.settings_keys[i]]:
                        flags = pygame.FULLSCREEN
                    pygame.display.set_mode((con.WIDTH, con.HEIGHT), flags)
            self.update_button(button, i)

    def update_button(self, button, btn_index):
        """Function for updating button"""
        button_state = self.game.settings[self.settings_keys[btn_index]]
        button_text = self.settings_labels[btn_index]+('ON' if button_state else 'OFF')
        button.draw_text(button_text)
        self.surf.blit(button.surf, button.rect)

"""15 puzzle"""

from random import randint
import copy
import pygame
import pygame.locals as loc
from .. import constants as con
from . import tile as t

class FifteenPuzzle(pygame.sprite.Sprite):
    """15 puzzle class"""

    def __init__(self, level, level_map):
        super().__init__()
        self.level = level
        gap = con.FIFTEEN_GAP
        self.gap = gap
        t_width, t_height = con.FIFTEEN_TILE_SIZE
        self.done = False

        self.level_map = []
        with open(level_map) as file:
            lines = file.read().strip().splitlines(False)
            for line in lines:
                tilestrings = line.split(',')
                row = []
                for t_string in tilestrings:
                    row.append(tuple(t_string.strip()))
                self.level_map.append(row)

        size = len(self.level_map[0]), len(self.level_map)
        self.size = size
        width, height = size
        self.surf = pygame.Surface((size[0]*(t_width+gap)+gap, size[1]*(t_height+gap)+gap))
        self.rect = self.surf.get_rect(center=(con.WIDTH//2, con.HEIGHT//2))
        self.board = []
        for tile_y in range(height):
            row = []
            for tile_x in range(width):
                row.append(t.Tile(level, size, self.level_map[tile_y][tile_x], (tile_x, tile_y)))
            self.board.append(row)
        self.board[-1][-1].empty = True
        self.move = pygame.mixer.Sound(con.FIFTEEN_MOVE)

        swaps = 0
        for tile_y, row in enumerate(self.board):
            for tile_x, tile in enumerate(row):
                rand_x, rand_y = tile_x, tile_y
                while rand_x == tile_x and rand_y == tile_y:
                    rand_x = randint(0, width-1)
                    rand_y = randint(0, height-1)
                if not (tile.swapped and self.board[rand_y][rand_x].swapped):
                    self.swap((tile_x, tile_y), (rand_x, rand_y))
                    self.board[tile_y][tile_x].swapped = True
                    self.board[rand_y][rand_x].swapped = True
                    swaps += 1
        emp_x, emp_y = find_empty(self.board)
        if ((width-emp_x)+(height-emp_y)+swaps) % 2 != 0:
            self.swap((0, 0), (1, 1))

    def swap(self, coords1, coords2):
        """Function for swapping tiles"""
        act_x, act_y = coords1
        swap_x, swap_y = coords2
        temp = copy.copy(self.board[act_y][act_x])
        self.board[act_y][act_x] = copy.copy(self.board[swap_y][swap_x])
        self.board[swap_y][swap_x] = temp
        self.board[act_y][act_x].rect.topleft = (
            act_x*(self.board[act_y][act_x].rect.width+self.gap)+self.gap,
            act_y*(self.board[act_y][act_x].rect.height+self.gap)+self.gap
        )
        self.board[swap_y][swap_x].rect.topleft = (
            swap_x*(self.board[swap_y][swap_x].rect.width+self.gap)+self.gap,
            swap_y*(self.board[swap_y][swap_x].rect.height+self.gap)+self.gap
        )

    def update(self, act_events):
        """Function for updating 15 puzzle"""
        self.surf.fill(con.FIFTEEN_BACK)
        moved = False
        for act_y, row in enumerate(self.board):
            for act_x, tile in enumerate(row):
                for event in act_events:
                    if event.type == loc.KEYDOWN:
                        swaps = [(act_x, act_y-1), (act_x, act_y+1),
                            (act_x-1, act_y), (act_x+1, act_y)]
                        keys = [loc.K_UP, loc.K_DOWN, loc.K_LEFT, loc.K_RIGHT]
                        for (swap_x, swap_y), key in zip(swaps, keys):
                            if not (0 <= swap_x < self.size[0] and 0 <= swap_y < self.size[1]):
                                continue
                            if event.key == key and self.board[swap_y][swap_x].empty:
                                if self.level.game.settings['sounds']:
                                    self.move.play()
                                self.swap((act_x, act_y), (swap_x, swap_y))
                                moved = True
                                break
                    if moved:
                        break
                if moved:
                    break
            if moved:
                break
        for row in self.board:
            for tile in row:
                if not tile.empty:
                    self.surf.blit(tile.surf, tile.rect)
        done = True
        for tile_y, row in enumerate(self.board):
            for tile_x, tile in enumerate(row):
                if tile.number != tile_y*self.size[0] + tile_x+1:
                    done = False
        if done and not self.done:
            for row in self.board:
                for tile in row:
                    tile.change_image()
            self.done = True

def find_empty(board):
    """Function for finding empty tile in board"""
    for y_pos, row in enumerate(board):
        for x_pos, tile in enumerate(row):
            if tile.empty:
                return (x_pos, y_pos)
    return (None, None)

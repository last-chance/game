"""15 puzzle tile"""

import os
import pygame
from .. import constants as con

class Tile(pygame.sprite.Sprite):
    """Tile class"""

    def __init__(self, level, board_size, image_data, coords, empty=False):
        super().__init__()
        self.level = level
        self.empty = empty
        self.number = coords[1]*board_size[0] + coords[0]+1
        width, height = con.FIFTEEN_TILE_SIZE
        gap = con.FIFTEEN_GAP
        self.surf = pygame.Surface((width, height))
        self.rect = self.surf.get_rect(topleft=(coords[0]*(width+gap), coords[1]*(height+gap)))
        img_dir = con.FIFTEEN_TILES
        self.images = [
            pygame.image.load(os.path.join(img_dir, 'disabled/', str(image_data[0])+'.png')),
            pygame.image.load(os.path.join(img_dir, 'enabled/', str(image_data[0])+'.png'))
        ]
        for img_index, img in enumerate(self.images):
            self.images[img_index] = pygame.transform.rotate(img, -int(image_data[1])*90)
        self.font = self.level.my_font
        self.text = self.font.render(str(self.number), con.ANTIALIAS, con.FIFTEEN_TEXT_C)
        self.text_rect = self.text.get_rect(topright=(self.rect.width-5, 5))
        self.image_index = 0
        self.surf.blit(self.images[self.image_index], (0, 0))
        self.surf.blit(self.text, self.text_rect)
        self.swapped = False

    def change_image(self):
        """Function for changing tile image"""
        self.image_index = (self.image_index+1) % len(self.images)
        self.surf.blit(self.images[self.image_index], (0, 0))
        self.surf.blit(self.text, self.text_rect)

    def __str__(self):
        return f'<Tile num={self.number} empty={self.empty}>'

"""Level that contains 15 puzzles"""

import os
import pygame
from . import fifteen_puzzle as f
from .. import constants as con
from .. import level as lvl
from .. import level_help as h

class FifteenLevel(lvl.Level):
    """Class for level that contains 15 puzzles"""

    def __init__(self, game):
        super().__init__(game)
        self.sound_played = False
        self.my_font = pygame.font.Font(con.HEADER_FONT, con.FIFTEEN_TEXT_SIZE)
        self.background = pygame.image.load(con.PUZZLE_BACK)
        self.help = h.Help(self.game, con.FIFTEEN_HELP)

        self.levels = []
        for f_lvl in sorted(os.listdir(con.FIFTEEN_LEVELS)):
            level = f.FifteenPuzzle(self, os.path.join(con.FIFTEEN_LEVELS, f_lvl))
            level.update([])
            self.levels.append(level)

    def update(self, events, _=None):
        """Function for updating 15 puzzle level"""
        if not pygame.mixer.music.get_busy() and self.game.settings['music']:
            pygame.mixer.music.load(con.PUZZLE_MUSIC)
            pygame.mixer.music.play(-1)
        level = self.levels[self.game.settings['sublevel']]
        self.surf.blit(self.background, (0, 0))
        if level.done:
            if not self.sound_played and self.game.settings['sounds']:
                self.done_sound.play()
                self.sound_played = True
            self.surf.blit(self.next_button.surf, self.next_button.rect)
            if self.next_button.clicked(events):
                self.sound_played = False
                if self.game.settings['sublevel'] < len(self.levels)-1:
                    self.game.settings['sublevel'] += 1
                elif self.game.settings['level'] < len(self.game.levels)-1:
                    self.game.settings['sublevel'] = 0
                    self.game.settings['level'] += 1
                    pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
                else:
                    self.game.menu.display = True
        self.help.update(events)
        if not self.help.display:
            level.update(events)
        self.surf.blit(level.surf, level.rect)
        self.surf.blit(self.help.surf, self.help.rect)

"""Bullet module"""

import pygame
from .. import constants as con

class Bullet(pygame.sprite.Sprite):
    """Bullet class"""

    def __init__(self, game, shooter, speed=300):
        super().__init__()
        self.game = game
        self.shooter = shooter
        self.direction = shooter.direction
        self.image = pygame.image.load(shooter.bullet_img)
        self.surf = pygame.Surface(shooter.bullet_size, pygame.SRCALPHA)
        self.rect = self.surf.get_rect(center=shooter.rect.center)
        self.surf.blit(self.image, (0, 0))
        if self.direction:
            self.surf = pygame.transform.rotate(self.surf, 180)
        self.speed = speed

    def update(self, clock):
        """Function for updating bullet's position"""
        pix = self.speed//get_fps(clock)
        self.rect.move_ip(0, pix if self.direction else -pix)
        for collide_shooter in pygame.sprite.spritecollide(self, self.game.shooters, False):
            if collide_shooter != self.shooter:
                self.kill()
                collide_shooter.kill()

def get_fps(my_clock):
    """Function that returns actual FPS"""
    act_fps = my_clock.get_fps()
    return act_fps if act_fps else con.FPS

"""Shooter game"""

from random import randrange
import pygame
from .. import constants as con
from . import player as p
from . import enemy as e

class ShooterGame(pygame.sprite.Sprite):
    """
    Class for shooter game.
    Image data: 'anim', 'shooting' (optionally), 'size';
    Bullet data: 'image', 'size'
    """

    def __init__(self, level, coords=(con.WIDTH//2, con.HEIGHT//2), size=(con.WIDTH, con.HEIGHT)):
        super().__init__()
        self.level = level
        self.background = pygame.image.load(con.SHOOTER_BACK)
        self.foregrounds = []
        for img in [con.SHOOTER_FG1, con.SHOOTER_FG2]:
            image = pygame.image.load(img).convert()
            image.set_colorkey((255, 255, 255, 0))
            self.foregrounds.append(image)
        self.tentacles = pygame.Surface(self.foregrounds[0].get_size(), pygame.SRCALPHA)
        self.tentacles.set_colorkey((255, 255, 255))
        self.surf = pygame.Surface(size)
        self.rect = self.surf.get_rect(center=coords)
        self.shooters = pygame.sprite.Group()
        self.enemies = pygame.sprite.Group()
        self.enemy_count = 0
        self.state = 0

        self.player = p.Player(self, (size[0]//2, 5*size[1]//6), False)
        self.shooters.add(self.player)

        for en_y in range(0, 150, 50):
            for en_x in range(size[0]//8, 7*size[0]//8, 100):
                anim_time = randrange(2000, 5000)
                enemy = e.Enemy(self, (en_x+(en_y//2)%100, 100+en_y), True, shot_gap=anim_time,
                    move_gap=randrange(200, 2000))
                self.enemy_count += 1
                self.shooters.add(enemy)
                self.enemies.add(enemy)

    def update(self, my_clock):
        """Function for updating game"""
        if self.background:
            self.surf.blit(self.background, (0, 0))
        else:
            self.surf.fill((80, 80, 80))

        for shooter_spr in self.shooters:
            shooter_spr.update(my_clock)
            self.surf.blit(shooter_spr.surf, shooter_spr.rect)

        if self.foregrounds:
            alpha = 255-int(len(self.enemies)/self.enemy_count*255)
            self.tentacles.fill((0, 0, 0, 0))
            self.foregrounds[1].set_alpha(alpha)
            self.tentacles.blits(zip(self.foregrounds, [(0, 0), (0, 0)]))
            self.surf.blit(self.tentacles, (0, 0))

        if not self.player.alive():
            self.state = 2
        elif not self.enemies:
            self.state = 1

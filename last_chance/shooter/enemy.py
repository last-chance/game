"""Enemy"""

import pygame
from .. import constants as con
from . import shooter

class Enemy(shooter.Shooter):
    """Enemy class"""

    def __init__(self, game, coords, direction, move_gap=1000, move_force=5, shot_gap=3000):
        super().__init__(game, con.SH_ENEMY_IMAGE_DATA['anim'], con.SH_ENEMY_IMAGE_DATA['size'],
            coords, con.SH_ENEMY_BULLET['image'], con.SH_ENEMY_BULLET['size'], direction,
            img_time=shot_gap//2, shot_gap=shot_gap)
        self.move_force = move_force
        self.move_time = pygame.time.get_ticks()
        self.move_gap = move_gap

    def update(self, my_clock):
        """Function for updating enemy"""
        super().update(my_clock)
        if self.rect.bottom >= self.game.player.rect.top:
            self.game.player.kill()
        time = pygame.time.get_ticks()
        if time - self.move_time >= self.move_gap:
            self.rect.move_ip(0, self.move_force)
            self.move_time = time
        self.shoot()

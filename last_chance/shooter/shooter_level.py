"""Shooter level"""

import pygame
import pygame.locals as loc
from .. import constants as con
from .. import button as btn
from .. import level as lvl
from .. import level_help as h
from . import shooter_game as sh

class ShooterLevel(lvl.Level):
    """Shooter level class"""

    def __init__(self, game):
        self.surf = None
        self.rect = None
        super().__init__(game)
        self.my_font = pygame.font.Font(con.HEADER_FONT, con.FIFTEEN_TEXT_SIZE)
        self.cover = pygame.Surface((con.WIDTH, con.HEIGHT), pygame.SRCALPHA)
        self.game_over_text = self.my_font.render('Game Over', con.ANTIALIAS, con.SHOOTER_TEXT_C)
        self.game_over_rect = self.game_over_text.get_rect(center=(con.WIDTH//2, con.HEIGHT//2))
        self.retry_button = btn.Button('Retry')
        self.retry_button.rect.center = (con.WIDTH//2, con.HEIGHT//3*2)
        self.cover.fill((0, 0, 0, 150))
        self.cover.blit(self.game_over_text, self.game_over_rect)
        self.cover.blit(self.retry_button.surf, self.retry_button.rect)
        self.game_over_sound = pygame.mixer.Sound(con.SHOOTER_GAME_OVER)
        self.game_over = False
        self.help = h.Help(self.game, con.SHOOTER_HELP)
        self.shooter_game = sh.ShooterGame(self)
        self.shooter_game.update(pygame.time.Clock())

    def update(self, events, clock):
        """Function for updating shooter game level"""
        if not pygame.mixer.music.get_busy() and self.game.settings['music']:
            pygame.mixer.music.load(con.SHOOTER_MUSIC)
            pygame.mixer.music.play(-1)
        if self.shooter_game.state == 1:
            if self.game.settings['sounds']:
                self.done_sound.play()
            pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
            if self.game.settings['level'] < len(self.game.levels)-1:
                self.game.settings['level'] += 1
            else:
                self.game.menu.display = True
        elif self.shooter_game.state == 2:
            if not self.game_over:
                if self.game.settings['sounds']:
                    self.game_over_sound.play()
                self.game_over = True
                self.surf.blit(self.cover, (0, 0))
            space_pressed = False
            for e in events:
                if e.type == loc.KEYDOWN and e.key == loc.K_SPACE:
                    space_pressed = True
            if self.retry_button.clicked(events) or space_pressed:
                self.game_over = False
                self.shooter_game = sh.ShooterGame(self)
        else:
            if not self.help.display:
                self.shooter_game.update(clock)
            self.surf.blit(self.shooter_game.surf, self.shooter_game.rect)
            self.help.update(events)
            self.surf.blit(self.help.surf, self.help.rect)

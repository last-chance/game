"""Shooter module"""

import os
import pygame
from . import bullet

class Shooter(pygame.sprite.Sprite):
    """Shooter class"""

    def __init__(self, game, image_dir, size, coords, bullet_img, bullet_size,
            direction, img_time=200, shot_gap=500):
        super().__init__()
        self.game = game
        self.images = []
        for img in sorted(os.listdir(image_dir)):
            self.images.append(pygame.image.load(os.path.join(image_dir, img)))
        self.surf = pygame.Surface(size, pygame.SRCALPHA)
        self.rect = self.surf.get_rect(center=coords)
        self.img_time = img_time
        self.direction = direction
        self.bullet_img = bullet_img
        self.bullet_size = bullet_size
        self.bullets = pygame.sprite.Group()
        self.shot_time = pygame.time.get_ticks()
        self.shot_gap = shot_gap

    def shoot(self):
        """Function for shooting"""
        time = pygame.time.get_ticks()
        if time - self.shot_time >= self.shot_gap:
            self.bullets.add(bullet.Bullet(self.game, self))
            self.shot_time = time

    def update(self, my_clock):
        """Function for updating shooter"""
        self.surf.fill((0, 0, 0, 0))
        time = pygame.time.get_ticks()
        self.surf.blit(self.images[time//self.img_time % len(self.images)], (0, 0))
        for my_bullet in self.bullets:
            my_bullet.update(my_clock)
            self.game.surf.blit(my_bullet.surf, my_bullet.rect)

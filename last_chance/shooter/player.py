"""Player"""

import pygame
import pygame.locals as loc
from .. import constants as con
from . import shooter

class Player(shooter.Shooter):
    """Player class"""

    def __init__(self, game, coords, direction, speed=300):
        super().__init__(game, con.SH_PLAYER_IMAGE_DATA['anim'], con.SH_PLAYER_IMAGE_DATA['size'],
            coords, con.SH_PLAYER_BULLET['image'], con.SH_PLAYER_BULLET['size'], direction)
        self.speed = speed
        self.shooting_img = pygame.image.load(con.SH_PLAYER_IMAGE_DATA['shooting'])
        self.shoot_sound = pygame.mixer.Sound(con.SHOOTER_PLAYER_SHOOT)

    def update(self, my_clock):
        """Function for updating player"""
        super().update(my_clock)
        pressed = pygame.key.get_pressed()
        act_fps = get_fps(my_clock)
        if pressed[loc.K_RIGHT] and self.rect.right < self.game.rect.width:
            self.rect.move_ip(int(self.speed/act_fps), 0)
        if pressed[loc.K_LEFT] and self.rect.left > 0:
            self.rect.move_ip(-int(self.speed/act_fps), 0)
        if pressed[loc.K_SPACE]:
            if self.shooting_img:
                self.surf.fill((0, 0, 0, 0))
                self.surf.blit(self.shooting_img, (0, 0))
            time = pygame.time.get_ticks()
            if time - self.shot_time >= self.shot_gap:
                if self.game.level.game.settings['sounds']:
                    self.shoot_sound.play()
                self.shoot()

def get_fps(my_clock):
    """Function that returns actual FPS"""
    act_fps = my_clock.get_fps()
    return act_fps if act_fps else con.FPS

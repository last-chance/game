"""Button"""

import pygame
import pygame.locals as loc
from . import constants as con

class Button():
    """Class for button"""

    def __init__(self, text, size=(0, 0), coords=(0, 0), background=con.NEXT_BTN_BACK,
            font=con.HEADER_FONT, font_size=con.NEXT_BTN_TEXT_SIZE, font_color=con.NEXT_BTN_TEXT_C):
        self.font = pygame.font.Font(font, font_size)
        self.given_size = size
        self.background = background
        self.font_color = font_color
        self.surf = None
        self.rect = None
        self.coords = coords
        self.draw_text(text)

    def draw_text(self, text):
        """Draw text to button"""
        self.text = text
        size = self.given_size
        text_size = self.font.size(text)
        surf_size = [max([size[0], text_size[0]+10]), max([size[1], text_size[1]+10])]
        self.surf = pygame.Surface(surf_size, pygame.SRCALPHA)
        self.rect = self.surf.get_rect(center=(self.coords))
        self.text = self.font.render(text, con.ANTIALIAS, self.font_color)
        self.text_rect = self.text.get_rect(center=(self.rect.width//2, self.rect.height//2))
        self.surf.fill(self.background)
        self.surf.blit(self.text, self.text_rect)

    def clicked(self, act_events, origin=(0, 0)):
        """Test if button is clicked"""
        for e in act_events:
            if e.type == loc.MOUSEBUTTONDOWN:
                click_pos = (e.pos[0]-origin[0], e.pos[1]-origin[1])
                if self.rect.collidepoint(click_pos):
                    return True
        return False

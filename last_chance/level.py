"""Level"""

import pygame
from . import constants as con
from . import button as btn

class Level():
    """Level class"""

    def __init__(self, game):
        self.game = game
        self.surf = pygame.Surface(self.game.surf.get_size(), pygame.SRCALPHA)
        self.rect = self.surf.get_rect(topleft=(0, 0))
        self.next_button = btn.Button('Next ->')
        self.next_button.rect.bottomright = (con.WIDTH-10, con.HEIGHT-10)
        self.done_sound = pygame.mixer.Sound(con.DONE_SOUND)
        self.sound_played = False

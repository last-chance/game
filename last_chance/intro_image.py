"""Introduction image player"""

import os
import pygame
import pygame.locals as loc
from . import constants as con

class IntroImage(pygame.sprite.Sprite):
    """Intro image class"""

    def __init__(self, image, coords=(0, 0),
            time=3000, intro_time=1000, outro_time=1000):
        super().__init__()
        self.time = time
        self.intro_time = intro_time
        self.outro_time = outro_time
        self.image = pygame.image.load(image)
        self.image_rect = self.image.get_rect()
        self.surf = pygame.Surface((self.image_rect.width, self.image_rect.height), pygame.SRCALPHA)
        self.rect = self.surf.get_rect(center=coords)
        self.start_time = None
        self.done = False

    def update(self):
        """Function for image update"""
        act_time = pygame.time.get_ticks()
        if self.start_time is None:
            self.start_time = act_time
        img_act_time = act_time-self.start_time
        if img_act_time <= self.time:
            self.done = False
            alpha = 255
            if img_act_time <= self.intro_time:
                alpha = int(img_act_time / self.intro_time * 256)
            elif img_act_time > self.time-self.outro_time:
                outro_start = self.time-self.outro_time
                alpha = 255-int((img_act_time-outro_start) / (self.time-outro_start) * 256)
            if alpha < 0:
                alpha = 0
            if alpha > 255:
                alpha = 255
            cover = pygame.Surface(self.surf.get_size(), pygame.SRCALPHA)
            cover.fill((255, 255, 255, alpha))
            self.surf.fill((0, 0, 0, 0))
            self.surf.blit(self.image, (0, 0))
            self.surf.blit(cover, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
        else:
            self.surf.fill((0, 0, 0, 0))
            self.done = True

class IntroImageSeries(pygame.sprite.Sprite):
    """Series from intro images"""

    def __init__(self, game, image_dir, music=None, music_loop=False, gap=2000, gaps=None,
            coords=(con.WIDTH//2, con.HEIGHT//2), time=6000, times=None,
            intro_time=1500, outro_time=1500, fades=None):
        self.game = game
        self.gap = gap
        self.gaps = gaps if gaps else []
        times = times if times else []
        fades = fades if fades else[]
        self.update_time = 0
        self.surf = None
        self.rect = None
        self.music_playing = False
        self.music_loop = music_loop
        self.music = music
        self.images = []
        for i, img in enumerate(sorted(os.listdir(image_dir))):
            intro_img = IntroImage(os.path.join(image_dir, img), coords,
                times[i] if i < len(times) else time,
                fades[i] if i < len(fades) else intro_time,
                fades[i] if i < len(fades) else outro_time)
            intro_img.rect.center = coords
            self.images.append(intro_img)

    def update(self, events, _):
        """Function for updating series of intro images"""
        if not pygame.mixer.music.get_busy() and self.game.settings['music']:
            pygame.mixer.music.load(self.music)
            pygame.mixer.music.play(-1)
            self.music_playing = True
        time = pygame.time.get_ticks()
        image = self.images[self.game.settings['sublevel']]
        image.update()
        if self.game.settings['sublevel'] < len(self.gaps):
            gap = self.gaps[self.game.settings['sublevel']]
        else:
            gap = self.gap
        self.surf = image.surf
        self.rect = self.images[self.game.settings['sublevel']].rect
        if image.done and time-self.update_time-image.time >= gap:
            self.update_time = time
            if self.game.settings['sublevel'] < len(self.images)-1:
                self.game.settings['sublevel'] += 1
            else:
                self.quit()
        for e in events:
            if e.type == loc.KEYDOWN and e.key in [loc.K_SPACE, loc.K_ESCAPE]:
                self.quit()

    def quit(self):
        """Function for quitting intro image series"""
        if self.game.settings['level'] < len(self.game.levels)-1:
            self.game.settings['sublevel'] = 0
            self.game.settings['level'] += 1
            pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
            for i in self.images:
                i.start_time = None
        else:
            pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
            self.game.menu.display = True

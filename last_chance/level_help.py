"""Help window"""

import os
import re
import pygame
import pygame.locals as loc
from . import constants as con
from . import button as btn

class Help(pygame.sprite.Sprite):
    """Class for help window"""

    def __init__(self, game, content, size=(2*con.WIDTH//3, 2*con.HEIGHT//3)):
        super().__init__()
        self.game = game
        gap = con.HELP_GAP
        self.display = True
        self.surf = pygame.Surface((con.WIDTH, con.HEIGHT), pygame.SRCALPHA)
        self.rect = self.surf.get_rect(topleft=(0, 0))
        self.t_font = pygame.font.Font(con.TEXT_FONT, con.HELP_T_SIZE)
        self.h_font = pygame.font.Font(con.HEADER_FONT, con.HELP_H_SIZE)
        self.button = btn.Button('?', background=(0, 0, 0, 0))
        self.button.rect.topright = (self.game.menu_btn.rect.left-gap, 5)
        self.content_surf = pygame.Surface(size)
        self.content_rect = self.content_surf.get_rect(center=(con.WIDTH//2, con.HEIGHT//2))
        self.content_surf.fill(con.HELP_BACK)
        self.content = []
        with open(content, encoding='utf-8') as file:
            text = file.read().strip()
            images = [img[1:-1] for img in re.findall('<.*>', text)]
            text_blocks = map(lambda string: string.strip(), re.split('<.*>', text))
        y = gap
        for i, block in enumerate(text_blocks):
            block_lines = block.splitlines(False)
            for line in block_lines:
                if line and line[0] == '#':
                    line = line.replace('#', '').strip()
                    line_surf = self.h_font.render(line, con.ANTIALIAS, con.HELP_FONT_C)
                else:
                    line_surf = self.t_font.render(line, con.ANTIALIAS, con.HELP_FONT_C)
                self.content_surf.blit(line_surf, (gap, y))
                y += line_surf.get_size()[1]
            y += gap
            if i < len(images):
                img_path = os.path.join(os.path.split(content)[0], images[i])
                image_surf = pygame.image.load(img_path)
                self.content_surf.blit(image_surf, (gap, y))
                y += image_surf.get_size()[1]+gap
        self.cross = btn.Button('×', background=(0, 0, 0, 0), font=con.TEXT_FONT,
            font_size=con.HELP_T_SIZE, font_color=con.HELP_FONT_C)
        self.cross.rect.topright = (self.content_rect.width-gap, gap)
        self.content_surf.blit(self.cross.surf, self.cross.rect)
        self.update([])

    def update(self, events):
        """Function for updating help window"""
        content_origin = (self.content_rect.left, self.content_rect.top)
        if self.cross.clicked(events, origin=content_origin):
            self.display = False
        if self.button.clicked(events):
            self.display = True
        for e in events:
            if e.type == loc.KEYDOWN and e.key == loc.K_F1:
                self.display = not self.display
        if self.display:
            self.surf.fill((0, 0, 0, 128))
            self.surf.blit(self.content_surf, self.content_rect)
        else:
            self.surf.fill((0, 0, 0, 0))
            self.surf.blit(self.button.surf, self.button.rect)

"""Nonogram puzzle"""

import pygame
from . import pixel as pix
from .. import constants as con

class Nonogram(pygame.sprite.Sprite):
    """Class for nonogram puzzle"""

    def __init__(self, level, image):
        super().__init__()
        pixel_width = con.NONOGRAM_PIX_SIZE
        gap = con.NONOGRAM_PIX_GAP
        legend_gap = con.NONOGRAM_LEGEND_GAP
        self.level = level
        with open(image) as file:
            file_text = file.read()
        file_lines = file_text.strip().splitlines(False)
        self.result = file_lines[0].strip()
        self.image = [[pix == '#' for pix in l] for l in file_lines[1:]]

        self.width = len(self.image[0])
        self.height = len(self.image)

        self.row_counts = [count_pixels(row) for row in self.image]
        columns = [[row[i] for row in self.image] for i in range(len(self.image[0]))]
        self.col_counts = [count_pixels(col) for col in columns]
        self.max_row_counts = max([len(row) for row in self.row_counts])
        self.max_col_counts = max([len(col) for col in self.col_counts])
        self.res_text = self.level.h_font.render(self.result, con.ANTIALIAS, con.NONOGRAM_TEXT_C)
        self.res_rect = self.res_text.get_rect()
        size = self.level.t_font.size('00')
        self.text_width = size[0]+legend_gap
        self.text_height = size[1]

        text_r_width = self.text_width*self.max_row_counts+legend_gap
        text_c_height = self.text_height*self.max_col_counts+legend_gap

        self.surf = pygame.Surface(
            (
                text_r_width + self.width*(pixel_width+gap)+legend_gap,
                text_c_height + self.height*(pixel_width+gap)+self.res_rect.height+2*legend_gap
            ),
            pygame.SRCALPHA
        )
        x, y = self.level.rect.center
        self.rect = self.surf.get_rect(bottomright=(
            x+self.width*(pixel_width+gap)//2+legend_gap,
            y+self.height*(pixel_width+gap)//2+self.res_rect.height+legend_gap
        ))
        self.res_rect.midbottom = (
            (self.rect.width-text_r_width)//2+text_r_width,
            self.rect.height-legend_gap
        )

        for num_y, row in enumerate(self.row_counts):
            for num_x, num in enumerate(row):
                text = self.level.t_font.render(str(num), con.ANTIALIAS, con.NONOGRAM_TEXT_C)
                text_rect = text.get_rect()
                text_rect.midright = (
                    int((self.max_row_counts-len(row)+num_x+1)*self.text_width),
                    int((num_y+0.5)*(pixel_width+gap)+text_c_height)
                )
                self.surf.blit(text, text_rect)
        for num_x, col in enumerate(self.col_counts):
            for num_y, num in enumerate(col):
                text = self.level.t_font.render(str(num), con.ANTIALIAS, con.NONOGRAM_TEXT_C)
                text_rect = text.get_rect()
                text_rect.midtop = (
                    int((num_x+0.5)*(pixel_width+gap)+text_r_width),
                    int((self.max_col_counts-len(col)+num_y)*self.text_height+legend_gap)
                )
                self.surf.blit(text, text_rect)

        self.pixels = []
        for pix_y in range(self.height):
            row = []
            for pix_x in range(self.width):
                pixel = pix.Pixel(
                    self.level,
                    (pixel_width, pixel_width),
                    (
                        text_r_width + pix_x*(pixel_width+gap),
                        text_c_height + pix_y*(pixel_width+gap)
                    )
                )
                row.append(pixel)
                self.surf.blit(pixel.surf, pixel.rect)
            self.pixels.append(row)
        self.done = False

    def update(self, act_events):
        """Function for updating nonogram"""
        for row in self.pixels:
            for pixel in row:
                pixel.update(act_events, self.rect.topleft)
                self.surf.blit(pixel.surf, pixel.rect)
        states = [[pix.state == 1 for pix in row] for row in self.pixels]
        if states == self.image and not self.done:
            self.surf.blit(self.res_text, self.res_rect)
            self.done = True

def count_pixels(pixel_list):
    """Function for counting pixels in row/column"""
    in_row = 0
    counted_list = []
    for pixel in pixel_list:
        if pixel:
            in_row += 1
        elif in_row != 0:
            counted_list.append(in_row)
            in_row = 0
    if in_row != 0:
        counted_list.append(in_row)
    if len(counted_list) == 0:
        counted_list.append(0)
    return counted_list

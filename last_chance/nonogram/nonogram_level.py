"""Level that contains nonograms"""

import os
import pygame
from . import nonogram as n
from .. import constants as con
from .. import level as lvl
from .. import level_help as h

class NonogramLevel(lvl.Level):
    """Class for level that contains nonograms"""

    def __init__(self, game):
        super().__init__(game)
        self.sound_played = False
        self.t_font = pygame.font.Font(con.TEXT_FONT, con.NONOGRAM_LEGEND_SIZE)
        self.h_font = pygame.font.Font(con.HEADER_FONT, con.NONOGRAM_RES_SIZE)
        self.background = pygame.image.load(con.PUZZLE_BACK)
        self.help = h.Help(self.game, con.NONOGRAM_HELP)

        self.nonograms = []
        for img in sorted(os.listdir(con.NONOGRAM_LEVELS)):
            nonogram = n.Nonogram(self, os.path.join(con.NONOGRAM_LEVELS, img))
            nonogram.update([])
            self.nonograms.append(nonogram)

    def update(self, events, _=None):
        """Function for updating level"""
        if not pygame.mixer.music.get_busy() and self.game.settings['music']:
            pygame.mixer.music.load(con.PUZZLE_MUSIC)
            pygame.mixer.music.play(-1)
        level = self.nonograms[self.game.settings['sublevel']]
        self.surf.blit(self.background, (0, 0))
        if level.done:
            if not self.sound_played and self.game.settings['sounds']:
                self.done_sound.play()
                self.sound_played = True
            self.surf.blit(self.next_button.surf, self.next_button.rect)
            if self.next_button.clicked(events):
                self.sound_played = False
                if self.game.settings['sublevel'] < len(self.nonograms)-1:
                    self.game.settings['sublevel'] += 1
                elif self.game.settings['level'] < len(self.game.levels)-1:
                    self.game.settings['sublevel'] = 0
                    self.game.settings['level'] += 1
                    pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
                else:
                    self.game.menu.display = True
        self.help.update(events)
        if not self.help.display:
            level.update(events)
        self.surf.blit(level.surf, level.rect)
        self.surf.blit(self.help.surf, self.help.rect)

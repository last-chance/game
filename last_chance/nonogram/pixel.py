"""Nonogram pixel"""

import pygame
import pygame.locals as loc
from .. import constants as con

class Pixel(pygame.sprite.Sprite):
    """Class for "pixel" in nonogram"""

    def __init__(self, level, size, coords, state=0):
        super().__init__()
        self.level = level
        self.state = state
        self.surf = pygame.Surface(size)
        images = ('disabled.png', 'enabled.png', 'crossed.png')
        self.images = [
            pygame.image.load(con.NONOGRAM_PIXELS+image) for image in images
        ]
        self.surf.blit(self.images[0], (0, 0))
        self.rect = self.surf.get_rect(topleft=coords)
        self.click = pygame.mixer.Sound(con.NONOGRAM_CLICK)

    def update(self, act_events, origin):
        """Function for updating pixel's state"""
        for act_event in act_events:
            if act_event.type == loc.MOUSEBUTTONDOWN and act_event.button in [1, 3]:
                clickpos = (act_event.pos[0]-origin[0], act_event.pos[1]-origin[1])
                if self.rect.collidepoint(clickpos):
                    if self.level.game.settings['sounds']:
                        self.click.play()
                    if self.state == 0:
                        if act_event.button == 1:
                            self.state = 1
                        elif act_event.button == 3:
                            self.state = 2
                    else:
                        self.state = 0
        self.surf.blit(self.images[self.state], (0, 0))

"""Constants"""

import pygame
import pygame.locals as loc

WIDTH  = 800
HEIGHT = 600

FPS = 60

SETTINGS = 'settings.json'

IMAGES = 'assets/images/'
MUSIC  = 'assets/music/'
SOUNDS = 'assets/sounds/'
LEVELS = 'assets/levels/'
FONTS  = 'assets/fonts/'
ANIM = 'assets/animations/'
HELPS = 'assets/helps/'

ICON = IMAGES+'icon.png'
LOGO = IMAGES+'logo.png'
HEADER_FONT = FONTS+'Orbitron-Medium.ttf'
TEXT_FONT = FONTS+'Roboto-Medium.ttf'
ANTIALIAS = True
MUSIC_FADEOUT = 500
DONE_SOUND = SOUNDS+'done.wav'

MENU_BACK = (10, 10, 10)
MENU_BTN_WIDTH = 200
MENU_FONT_SIZE = 25
MENU_FONT_C = (255, 255, 255)

HELP_T_SIZE = 15
HELP_H_SIZE = 20
HELP_FONT_C = (255, 255, 255)
HELP_BACK = (40, 40, 40)
HELP_GAP = 10

INTRO_ANIM = ANIM+'intro/'
INTRO_MUSIC = MUSIC+'intro.wav'
INTRO_TIMES = [6000, 6000, 6000, 8000, 5000]

OUTRO_ANIM = ANIM+'outro/'
OUTRO_GAPS = [500, 500, 1000, 1000, 1000, 2000]
OUTRO_TIMES = [4000, 4000, 4000, 5000, 9000, 5000]
OUTRO_FADES = [500, 500, 500, 1500, 2000, 1500]
OUTRO_MUSIC = MUSIC+'outro.wav'

NEXT_BTN_BACK = (0, 120, 200)
NEXT_BTN_TEXT_SIZE = 20
NEXT_BTN_TEXT_C = (255, 255, 255)

HELP_BACK_C = (30, 30, 30)
HELP_TEXT_C = (255, 255, 255)

PUZZLE_BACK = IMAGES+'puzzle_background.png'
PUZZLE_MUSIC = MUSIC+'puzzles.wav'

FIFTEEN_TILES = IMAGES+'15_puzzle/'
FIFTEEN_LEVELS = LEVELS+'15_puzzle/'
FIFTEEN_HELP = HELPS+'15_puzzle/15_puzzle.txt'
FIFTEEN_MOVE = SOUNDS+'15_puzzle_move.wav'
FIFTEEN_GAP = 3
FIFTEEN_TILE_SIZE = (120, 120)
FIFTEEN_TEXT_SIZE = 25
FIFTEEN_TEXT_C = (255, 255, 255)
FIFTEEN_BACK = (100, 100, 100)

NONOGRAM_PIXELS = IMAGES+'nonograms/'
NONOGRAM_LEVELS = LEVELS+'nonograms/'
NONOGRAM_HELP = HELPS+'nonograms/nonograms.txt'
NONOGRAM_CLICK = SOUNDS+'nonogram_click.wav'
NONOGRAM_PIX_SIZE = 20
NONOGRAM_PIX_GAP = 1
NONOGRAM_LEGEND_GAP = 3
NONOGRAM_LEGEND_SIZE = 10
NONOGRAM_RES_SIZE = 25
NONOGRAM_TEXT_C = (255, 255, 255)

SHOOTER_PLAYER = IMAGES+'shooter/player/'
SHOOTER_ENEMY = IMAGES+'shooter/enemy/'
SHOOTER_BACK = IMAGES+'shooter/background.png'
SHOOTER_FG1 = IMAGES+'shooter/tentacles_red.png'
SHOOTER_FG2 = IMAGES+'shooter/tentacles_green.png'
SHOOTER_MUSIC = MUSIC+'shooter.wav'
SHOOTER_HELP = HELPS+'shooter/shooter.txt'
SHOOTER_PLAYER_SHOOT = SOUNDS+'shooter_p_shoot.wav'
SHOOTER_GAME_OVER = SOUNDS+'shooter_game_over.wav'
SHOOTER_TEXT_C = (255, 255, 255)
SHOOTER_TEXT_SIZE = 35
SH_PLAYER_IMAGE_DATA = {
    'anim'    : SHOOTER_PLAYER+'/anim',
    'shooting': SHOOTER_PLAYER+'/virophage_shooting.png',
    'size'    : (27, 27)
}
SH_PLAYER_BULLET = {
    'image': SHOOTER_PLAYER+'/bullet.png',
    'size' : (3, 6)
}
SH_ENEMY_IMAGE_DATA = {
    'anim': SHOOTER_ENEMY+'/anim',
    'size': (27, 27)
}
SH_ENEMY_BULLET = {
    'image': SHOOTER_ENEMY+'/bullet.png',
    'size' : (3, 6)
}

QUIT_EVENT = pygame.event.Event(loc.QUIT)

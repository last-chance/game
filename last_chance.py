"""Main program"""

import sys
import json
import pygame
import pygame.locals as loc
from last_chance import constants as con
from last_chance import intro_image as intro_img
from last_chance import menu
from last_chance import button as btn
from last_chance.nonogram import nonogram_level as nonograms
from last_chance.fifteen_puzzle import fifteen_level as fifteen
from last_chance.shooter import shooter_level as shooter

class Game():
    """Main game class"""

    def __init__(self, game_screen):
        self.surf = game_screen
        self.settings = load_settings()
        self.controll_settings = self.settings.copy()
        self.menu_btn = btn.Button('Menu', background=(0, 0, 0, 0))
        self.menu_btn.rect.topright = (con.WIDTH-10, 5)
        self.menu = menu.Menu(self)
        self.levels = [
            intro_img.IntroImageSeries(self, con.INTRO_ANIM, con.INTRO_MUSIC,
                times=con.INTRO_TIMES),
            nonograms.NonogramLevel(self),
            fifteen.FifteenLevel(self),
            shooter.ShooterLevel(self),
            intro_img.IntroImageSeries(self, con.OUTRO_ANIM, con.OUTRO_MUSIC, times=con.OUTRO_TIMES,
                gaps=con.OUTRO_GAPS, fades=con.OUTRO_FADES)
        ]

    def update(self, act_events):
        """Function for updating level"""
        self.surf.fill((0, 0, 0))
        level = self.levels[self.settings['level']]
        if self.menu_btn.clicked(act_events):
            self.menu.display = True
            pygame.mixer.music.fadeout(con.MUSIC_FADEOUT)
        if self.menu.display:
            self.menu.update(act_events)
            self.surf.blit(self.menu.surf, self.menu.rect)
        else:
            level.update(act_events, clock)
            self.surf.blit(level.surf, level.rect)
            self.surf.blit(self.menu_btn.surf, self.menu_btn.rect)

        if self.settings != self.controll_settings:
            dump_settings(self.settings)
            self.controll_settings = self.settings.copy()

def load_settings():
    """Function for loading settings"""
    with open(con.SETTINGS) as file:
        settings = json.load(file)
    return settings

def dump_settings(settings):
    """Function for dumping settings"""
    with open(con.SETTINGS, 'w') as file:
        file.write(json.dumps(settings, indent=4))

pygame.init()
clock = pygame.time.Clock()

flags = pygame.FULLSCREEN if load_settings()['fullscreen'] else 0
screen = pygame.display.set_mode((con.WIDTH, con.HEIGHT), flags)
pygame.display.set_caption('Last Chance')
icon = pygame.image.load(con.ICON)
pygame.display.set_icon(icon)

game = Game(screen)

while True:
    events = pygame.event.get()
    if any([e.type == loc.QUIT for e in events]):
        dump_settings(game.settings)
        pygame.quit()
        sys.exit()

    game.update(events)

    pygame.display.update()
    clock.tick(con.FPS)

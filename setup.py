"""Build exe"""

import os
from distutils.core import setup
from glob import glob
import py2exe

def get_all(path):
    """Get all files in dir"""
    r = [(os.path.split(y)[0], [y]) for x in os.walk(path) for y in glob(os.path.join(x[0], '*.*'))]
    return r


includes = ['last_chance', 'last_chance.nonogram', 'last_chance.fifteen_puzzle',
    'last_chance.shooter']
excludes = ['tkinter', 'numpy', 'zipimport', 'socket', 'asyncio']
data = [*get_all('assets/'), ('', ['./settings.json'])]

options = {
    'py2exe': {'includes': includes, 'excludes': excludes},
}
window = {
    'script': 'last_chance.py',
    'icon_resources': [
        (0, 'assets/images/icon.ico')
    ]
}

setup(
    name='Last Chance',
    version='1.1',
    description='Last chance to save the world...',
    author='Trabant Corporation',
    url='https://last-chance.gitlab.io',
    options=options, windows=[window], data_files=data
)
